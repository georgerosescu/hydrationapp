//
//  DailyIntakeGoal.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import Foundation
import RealmSwift

class DailyIntakeGoal: Object, ObjectKeyIdentifiable {
    
    ///An intake goal is unique for each day
    @Persisted(primaryKey: true) var id = Calendar.current.numberOfDaysSince1970(to: Date())
    @Persisted var goal = 2000
    @Persisted var currentIntake = 0
    @Persisted var measurementUnit: MeasurementUnit = .metric
    @Persisted var date = Date()
    
    //MARK: - Computed Properties
    var isDone: Bool { currentIntake >= goal}
    var donePercentString: String { "\(donePercent)%"}
    
    var todayIntakeValue: String { "\(currentIntake) \(measurementUnit.rawValue)" }
    var donePercent: Int { 100 * currentIntake / goal }
    var glassShapeFilledAmount: Double { donePercent >= 100 ? 1 : Double(donePercent) / 100.0 }
    var goalAndMeasurementUnit: String { "\(goal) \(measurementUnit.rawValue)"}
}
