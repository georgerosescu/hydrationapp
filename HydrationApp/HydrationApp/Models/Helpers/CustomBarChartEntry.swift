//
//  CustomBarChartEntry.swift
//  HydrationApp
//
//  Created by George Rosescu on 29.01.2022.
//

import Foundation
import Charts

class CustomBarChartEntry: BarChartDataEntry {
    var targetGoal: Int?
    var date: String = ""
    
    init(x: Double, y: Double, date: String, targetGoal: Int? = nil) {
        self.targetGoal = targetGoal
        self.date = date
        
        super.init(x: x, y: y)
    }
    
    required init() {
        super.init()
    }
    
    
    static func dummyEntry(x: Double, date: String) -> CustomBarChartEntry {
        CustomBarChartEntry(x: x, y: 200, date: date, targetGoal: nil)
    }
}
