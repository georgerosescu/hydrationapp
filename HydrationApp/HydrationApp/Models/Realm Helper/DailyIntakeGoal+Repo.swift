//
//  DailyIntakeGoal+Repo.swift
//  HydrationApp
//
//  Created by George Rosescu on 28.01.2022.
//

import Foundation
import RealmSwift

extension RealmManager {
    func create(_ todayGoal: DailyIntakeGoal) {
        let realm = try! Realm()
        try? realm.write {
            realm.add(todayGoal)
        }
    }
    
    var reverseSortedDailyGoals: [DailyIntakeGoal] {
        let realm = try! Realm()
        
        return realm.objects(DailyIntakeGoal.self).map { $0 }.sorted { $1.date < $0.date }
    }
    
    var todayGoal: DailyIntakeGoal? {
        let realm = try! Realm()
        let today = Calendar.current.numberOfDaysSince1970(to: Date())
        
        return realm.object(ofType: DailyIntakeGoal.self, forPrimaryKey: today)
    }
}
