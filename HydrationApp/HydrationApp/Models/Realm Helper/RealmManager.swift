//
//  RealmManager.swift
//  HydrationApp
//
//  Created by George Rosescu on 28.01.2022.
//

import Foundation
import RealmSwift

struct RealmManager {

    static let shared = RealmManager()
    
    private init() { }
    
    func writeTransaction(_ callback: @escaping ((Realm) -> Void)) {
        let realm = try! Realm()
        
        try? realm.write({
            callback(realm)
        })
    }
}
