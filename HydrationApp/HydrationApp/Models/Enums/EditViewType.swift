//
//  EditViewType.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import Foundation

enum EditViewType: String {
    case daily = "Daily Goal"
    case container1 = "Container1"
    case container2 = "Container2"
    case container3 = "Container3"
    
    var isContainer: Bool { [EditViewType.container1, EditViewType.container2, EditViewType.container3].contains(self) }
}
