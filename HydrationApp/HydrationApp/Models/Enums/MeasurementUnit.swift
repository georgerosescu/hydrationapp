//
//  MeasurementUnit.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import Foundation
import RealmSwift

enum MeasurementUnit: String, PersistableEnum, CaseIterable, Identifiable {
    var id: RawValue { rawValue }
    
    case metric = "ml"
    case imperial = "oz"
    
    var longTitle: String {
        switch self {
        case .imperial:
            return "ounces (oz)"
        case .metric:
            return "mililiters (ml)"
        }
    }
}
