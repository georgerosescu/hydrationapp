//
//  TodayViewModel.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import Foundation
import SwiftUI
import RealmSwift

class TodayViewModel: ObservableObject {
    @Published var todayIntake = DailyIntakeGoal()
    
    
    //MARK: - Actions
    func checkAndCreateTodayGoal() {
        
        //TODO: Uncomment this to add test data for chart
        //        if !Defaults.insertTest {
        //            for i in 1...9 {
        //                let dayComp = DateComponents(day: -i)
        //                let date = Calendar.current.date(byAdding: dayComp, to: Date())!
        //
        //                RealmManager.shared.writeTransaction { realm in
        //                    let intake = DailyIntakeGoal()
        //                    intake.id = Calendar.current.numberOfDaysSince1970(to: date)
        //                    intake.currentIntake = Int.random(in: 1000..<2500)
        //                    intake.goal = Int.random(in: 1700...2200)
        //                    intake.date = date
        //                    intake.measurementUnit = i % 2 == 0 ? .metric : .imperial
        //
        //                    realm.add(intake)
        //                }
        //            }
        //            Defaults.insertTest = true
        //        }
        
        if let todayGoal = RealmManager.shared.todayGoal {
            todayIntake = todayGoal
        } else {
            todayIntake.goal = Int(Defaults.dailyGoalValue) ?? Constants.defaultGoal
            todayIntake.measurementUnit = MeasurementUnit(rawValue: Defaults.measurementUnit) ?? .metric
            
            RealmManager.shared.create(todayIntake)
            fetchTodayGoal()
        }
    }
    
    func addIntakeAmount(from container: IntakeContainer) {
        var amount = 0
        switch container {
        case .first:
            amount = Int(Defaults.container1Value) ?? 0
        case .second:
            amount = Int(Defaults.container2Value) ?? 0
        case .third:
            amount = Int(Defaults.container3Value) ?? 0
        }
        
        RealmManager.shared.writeTransaction { [weak self] realm in
            guard let self = self else { return }
            
            self.todayIntake.currentIntake += amount
            self.fetchTodayGoal()
        }
    }
    
    func fetchTodayGoal() {
        if let todayIntake = RealmManager.shared.todayGoal {
            self.todayIntake = todayIntake
        }
    }
}

enum IntakeContainer {
    case first
    case second
    case third
}
