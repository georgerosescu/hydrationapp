//
//  GlobalViewModel.swift
//  HydrationApp
//
//  Created by George Rosescu on 29.01.2022.
//

import Foundation
import SwiftUI
import RealmSwift
import Charts

class GlobalViewModel: ObservableObject {
    @Published var historyGoals: [DailyIntakeGoal] = []
    
    //MARK: - Computed Props
    var barChartGoals: [CustomBarChartEntry] {
        var chartGoals = [CustomBarChartEntry]()
        let reverseSortedGoalsArray = Array(RealmManager.shared.reverseSortedDailyGoals.dropFirst().prefix(30))
        
        guard !reverseSortedGoalsArray.isEmpty else {
            for barIndex in 0...29 {
                var dateString = ""
                
                if let thirtyDaysAgo = Date().chartStartDate, barIndex == 0 {
                    dateString = thirtyDaysAgo.chartLegendFormatted()
                }
                
                if let thirtyDaysAgo = Date().yesterday, barIndex == 29 {
                    dateString = thirtyDaysAgo.chartLegendFormatted()
                }
                
                chartGoals.append(CustomBarChartEntry.dummyEntry(x: Double(barIndex), date: dateString))
            }
            
            return chartGoals
        }
        
        let uncompletedDays = 30 - reverseSortedGoalsArray.count
        for barIndex in 0..<uncompletedDays {
            var dateString = ""
            
            if barIndex == 0 {
                if let thirtyDaysAgo = Date().chartStartDate {
                    dateString = thirtyDaysAgo.chartLegendFormatted()
                }
            }
            
            chartGoals.append(CustomBarChartEntry.dummyEntry(x: Double(barIndex), date: dateString))
        }
        
        for barIndex in uncompletedDays...29 {
            var dateString = ""
            let index = 29 - barIndex
            
            if index < reverseSortedGoalsArray.count {
                if barIndex == 0 || barIndex == 29 {
                    dateString = reverseSortedGoalsArray[index].date.chartLegendFormatted()
                }
                
                chartGoals.append(CustomBarChartEntry(x: Double(barIndex), y: Double(reverseSortedGoalsArray[index].currentIntake), date: dateString, targetGoal: reverseSortedGoalsArray[index].goal))
            }
        }
        
        return chartGoals
    }
    
    //MARK: - Actions
    func fetchHistoryInkates() {
        historyGoals = RealmManager.shared.reverseSortedDailyGoals
        
        if historyGoals.count > 30 {
            historyGoals = Array(historyGoals[1...30])
        } else {
            historyGoals.removeFirst()
        }
    }
}
