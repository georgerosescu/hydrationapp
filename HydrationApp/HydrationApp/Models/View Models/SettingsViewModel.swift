//
//  SettingsViewModel.swift
//  HydrationApp
//
//  Created by George Rosescu on 28.01.2022.
//

import Foundation
import SwiftUI

class SettingsViewModel: ObservableObject {
    @Published var measurementUnit: MeasurementUnit
    @Published var dailyGoalValue: String
    
    @Published var container1Value: String
    @Published var container2Value: String
    @Published var container3Value: String
    
    var goalAndMeasurementUnit: String { "\(dailyGoalValue) \(measurementUnit.rawValue)"}
    
    var firstContainerReadableSize: String { "\(container1Value) \(measurementUnit.rawValue)" }
    var secondContainerReadableSize: String { "\(container2Value) \(measurementUnit.rawValue)" }
    var thirdContainerReadableSize: String { "\(container3Value) \(measurementUnit.rawValue)" }
    
    init() {
        measurementUnit = MeasurementUnit(rawValue: Defaults.measurementUnit) ?? .metric
        dailyGoalValue = Defaults.dailyGoalValue
        container1Value = Defaults.container1Value
        container2Value = Defaults.container2Value
        container3Value = Defaults.container3Value
    }
    
    //MARK: - Actions
    func saveMeasurement() {
        Defaults.measurementUnit = measurementUnit.rawValue
        guard let todayGoal = RealmManager.shared.todayGoal else { return }
        
        RealmManager.shared.writeTransaction { [weak self] _ in
            guard let self = self else { return }
            
            todayGoal.measurementUnit = self.measurementUnit
        }
    }
    
    func dismissChanges(for editType: EditViewType) {
        switch editType {
        case .daily:
            dailyGoalValue = Defaults.dailyGoalValue
        case .container1:
            container1Value = Defaults.container1Value
        case .container2:
            container2Value = Defaults.container2Value
        case .container3:
            container3Value = Defaults.container3Value
        }
    }
    
    func saveAmmount(for editType: EditViewType) {
        switch editType {
        case .daily:
            Defaults.dailyGoalValue = dailyGoalValue
            guard let todayGoal = RealmManager.shared.todayGoal else { return }
            
            RealmManager.shared.writeTransaction { [weak self] _ in
                guard let self = self else { return }
                
                todayGoal.goal = Int(self.dailyGoalValue) ?? 0
            }
        case .container1:
            Defaults.container1Value = container1Value
        case .container2:
            Defaults.container2Value = container2Value
        case .container3:
            Defaults.container3Value = container3Value
        }
    }
}
