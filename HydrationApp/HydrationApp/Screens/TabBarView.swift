//
//  TabBarView.swift
//  HydrationApp
//
//  Created by George Rosescu on 25.01.2022.
//

import SwiftUI

struct TabBarView: View {
    @StateObject var todayViewModel = TodayViewModel()
    @StateObject var globalViewModel = GlobalViewModel()
    
    var body: some View {
        TabView {
            TodayView(todayVM: todayViewModel).tabItem {
                VStack {
                    Image("today")
                        .renderingMode(.template)
                    Text("Home")
                }
            }
            
            HistoryView(globalViewModel: globalViewModel).tabItem {
                VStack {
                    Image("history")
                        .renderingMode(.template)
                    Text("History")
                }
            }
        }
        .accentColor(Color(uiColor: .customGreen))
    }
}

struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarView()
    }
}
