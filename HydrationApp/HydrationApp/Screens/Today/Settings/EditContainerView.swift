//
//  EditContainerView.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import SwiftUI

struct EditContainerView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var settingsViewMode: SettingsViewModel
    let editViewType: EditViewType
    
    var body: some View {
        intakeEditView
            .navigationBarTitleDisplayMode(.inline)
            .navigationTitle(editViewType.rawValue)
            .ignoresSafeArea(.keyboard, edges: .bottom)
            .navigationBarBackButtonHidden(true)
            .twoButtonsToolbar(leadingButtonTitle: "Cancel", trailingButtonTitle: "Save") {
                settingsViewMode.dismissChanges(for: editViewType)
                presentationMode.wrappedValue.dismiss()
            } trailingAction: {
                settingsViewMode.saveAmmount(for: editViewType)
                presentationMode.wrappedValue.dismiss()
            }
    }
    
    @ViewBuilder
    var intakeEditView: some View {
        switch editViewType {
        case .daily:
            IntakeEditView(editViewType: editViewType, measurementUnitTitle: settingsViewMode.measurementUnit.longTitle, value: $settingsViewMode.dailyGoalValue)
        case .container1:
            IntakeEditView(editViewType: editViewType, measurementUnitTitle: settingsViewMode.measurementUnit.longTitle, value: $settingsViewMode.container1Value)
        case .container2:
            IntakeEditView(editViewType: editViewType, measurementUnitTitle: settingsViewMode.measurementUnit.longTitle, value: $settingsViewMode.container2Value)
        case .container3:
            IntakeEditView(editViewType: editViewType, measurementUnitTitle: settingsViewMode.measurementUnit.longTitle, value: $settingsViewMode.container3Value)
        }
    }
}

struct EditContainerView_Previews: PreviewProvider {
    static var previews: some View {
        EditContainerView(settingsViewMode: SettingsViewModel(), editViewType: .container1)
    }
}
