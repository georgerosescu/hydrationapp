//
//  UnitsView.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import SwiftUI

struct UnitsView: View {
    @ObservedObject var settingsViewModel: SettingsViewModel
    
    var body: some View {
        Form {
            List {
                ForEach(MeasurementUnit.allCases) { unit in
                    MeasurementUnitCell(measurementUnit: unit, selectedMeasurementUnit: $settingsViewModel.measurementUnit)
                        .onTapGesture {
                            settingsViewModel.measurementUnit = unit
                        }
                }
            }
        }
        .onDisappear(perform: {
            settingsViewModel.saveMeasurement()
        })
        .navigationTitle("Units")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct UnitsView_Previews: PreviewProvider {
    static var previews: some View {
        UnitsView(settingsViewModel: SettingsViewModel())
            .preferredColorScheme(.dark)
    }
}
