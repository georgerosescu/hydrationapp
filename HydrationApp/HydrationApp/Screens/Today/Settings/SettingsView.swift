//
//  SettingsView.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import SwiftUI

struct SettingsView: View {
    @ObservedObject var settingsViewModel: SettingsViewModel
    @State private var selection: String? = nil
    
    var body: some View {
        Form {
            firstSection
            Section {
                EmptyView()
            }
            secondSection
        }
        .navigationTitle("Settings")
        .navigationBarTitleDisplayMode(.inline)
    }
    
    var firstSection: some View {
        Section(header: Text("")) {
            NavigationLink {
                UnitsView(settingsViewModel: settingsViewModel)
            } label: {
                SettingsCell(leftText: Constants.unitsTabTitle, rightText: settingsViewModel.measurementUnit.rawValue)
            }
            NavigationLink {
                EditContainerView(settingsViewMode: settingsViewModel, editViewType: .daily)
            } label: {
                SettingsCell(leftText: Constants.dailyGoalTabTitle, rightText: settingsViewModel.goalAndMeasurementUnit)
            }
        }
    }
    
    var secondSection: some View {
        Section {
            NavigationLink {
                EditContainerView(settingsViewMode: settingsViewModel, editViewType: .container1)
            } label: {
                SettingsCell(leftText: Constants.firstContainerTabTitle, rightText: settingsViewModel.firstContainerReadableSize)
            }
            NavigationLink {
                EditContainerView(settingsViewMode: settingsViewModel, editViewType: .container2)
            } label: {
                SettingsCell(leftText: Constants.secondContainerTabTitle, rightText: settingsViewModel.secondContainerReadableSize)
            }
            NavigationLink {
                EditContainerView(settingsViewMode: settingsViewModel, editViewType: .container3)
            } label: {
                SettingsCell(leftText: Constants.thirdContainerTabTitle, rightText: settingsViewModel.thirdContainerReadableSize)
            }
        } header: {
            Text(Constants.settingsListHeader)
                .foregroundColor(Color(uiColor: .customMediumGray))
                .font(.system(size: 14))
        } footer: {
            Text(Constants.settingsListFooter)
                .foregroundColor(Color(uiColor: .customMediumGray))
                .font(.system(size: 14))
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(settingsViewModel: SettingsViewModel())
            .preferredColorScheme(.dark)
    }
}
