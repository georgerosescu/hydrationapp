//
//  TodayView.swift
//  HydrationApp
//
//  Created by George Rosescu on 25.01.2022.
//

import SwiftUI

struct TodayView: View {
    @ObservedObject var todayVM: TodayViewModel
    @State private var selection: String? = nil
    
    var body: some View {
        NavigationView {
            ZStack {
                Image("leaf_bg")
                    .resizable()
                    .overlay(Color(uiColor: .black).opacity(0.4))
                VStack(alignment: .center, spacing: 40) {
                    GlassContainer(dailyGoal: todayVM.todayIntake)
                    intakesContainer
                    Text(Constants.welcomeText)
                        .foregroundColor(.white)
                        .font(.system(size: 14))
                        .multilineTextAlignment(.center)
                        .padding(.horizontal, 65)
                    
                    NavigationLink(tag: "settings", selection: $selection) {
                        SettingsView(settingsViewModel: SettingsViewModel())
                    } label: {
                        EmptyView()
                    }
                }
                .navigationTitle("Today's progress")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    Button {
                        selection = "settings"
                    } label: {
                        Image("settings")
                    }
                }
                .onAppear {
                    todayVM.checkAndCreateTodayGoal()
                }
            }
        }
    }
    
    var intakesContainer: some View {
        HStack {
            WaterIntakeCell(amount: "\(Defaults.container1Value) \(Defaults.measurementUnit)") {
                todayVM.addIntakeAmount(from: .first)
            }
            WaterIntakeCell(amount: "\(Defaults.container2Value) \(Defaults.measurementUnit)") {
                todayVM.addIntakeAmount(from: .second)
            }
            WaterIntakeCell(amount: "\(Defaults.container3Value) \(Defaults.measurementUnit)") {
                todayVM.addIntakeAmount(from: .third)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let todayVM = TodayViewModel()
        
        TodayView(todayVM: todayVM)
    }
}
