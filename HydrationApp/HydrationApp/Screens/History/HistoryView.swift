//
//  HistoryView.swift
//  HydrationApp
//
//  Created by George Rosescu on 25.01.2022.
//

import SwiftUI

struct HistoryView: View {
    @ObservedObject var globalViewModel: GlobalViewModel
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: true) {
                VStack {
                    Text(Constants.historyPageTitle)
                        .multilineTextAlignment(.center)
                        .foregroundColor(.white)
                        .font(.system(size: 14))
                        .padding(.horizontal, 60)
                    GoalsChart(chartData: globalViewModel.barChartGoals)
                        .frame(height: 400)
                        .padding(.bottom)
                    Spacer()
                    historyView
                }
                .navigationTitle("History")
                .navigationBarTitleDisplayMode(.inline)
                .padding(.horizontal, 15)
                .onAppear {
                    globalViewModel.fetchHistoryInkates()
                }
            }
        }
    }
    
    @ViewBuilder
    var historyView: some View {
        if !globalViewModel.historyGoals.isEmpty {
            ForEach(globalViewModel.historyGoals) {
                HistoryCell(date: $0.date, goal: $0.goalAndMeasurementUnit, actualAmount: $0.todayIntakeValue, donePercent: $0.donePercentString, isDone: $0.isDone)
                Divider()
                    .background(.white)
            }
        } else {
            Text("History list will be available tomorrow.")
                .foregroundColor(.white)
                .font(.system(size: 14))
        }
    }
}

struct HistoryView_Previews: PreviewProvider {
    static var previews: some View {
        HistoryView(globalViewModel: GlobalViewModel())
            .preferredColorScheme(.dark)
    }
}
