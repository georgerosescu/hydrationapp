//
//  HydrationAppApp.swift
//  HydrationApp
//
//  Created by George Rosescu on 25.01.2022.
//

import SwiftUI

@main
struct HydrationAppApp: App {
    var body: some Scene {
        WindowGroup {
            TabBarView()
                .preferredColorScheme(.dark)
        }
    }
}


