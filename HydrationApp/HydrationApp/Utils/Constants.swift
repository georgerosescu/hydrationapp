//
//  Constants.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import Foundation

struct Constants {
    
    static let defaultGoal = 2000
    static let dailyGoalUpperText = "Here you can set your hydratation goal based on your preferred unit of measurement"
    static let containerUpperText = "Here you can set your container size so it would be easier to enter your daily liquid intake"
    static let welcomeText = "Happy you are back to track your healthy habbit of staying hydrated."
    
    static let historyPageTitle = "Here tou can see your hydratation data for the last 30 days"
    
    //MARK: - Settings
    static let unitsTabTitle = "Units"
    static let dailyGoalTabTitle = "Daily Goal"
    static let firstContainerTabTitle = "Container 1"
    static let secondContainerTabTitle = "Container 2"
    static let thirdContainerTabTitle = "Container 3"
    
    static let settingsListHeader = "Containers"
    static let settingsListFooter = "These containers will appear on your main screen so you can easily tap on them and track your intake."
    
    struct Storage {
        static let measurementUnit = "measurementUnit"
        static let dailyGoalValue = "dailyGoalValue"
        static let container1Value = "container1Value"
        static let container2Value = "container2Value"
        static let container3Value = "container3Value"
    }
}
