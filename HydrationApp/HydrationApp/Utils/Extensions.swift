//
//  Extensions.swift
//  HydrationApp
//
//  Created by George Rosescu on 25.01.2022.
//

import Foundation
import SwiftUI
import UIKit


//MARK: - Array
extension Array where Element: CustomBarChartEntry {
    
    var maximumChartValue: Double {
        var maximumValue: Double = 0
        
        self.forEach {
            if $0.y > maximumValue {
                maximumValue = $0.y
            }
        }
        
        return maximumValue
    }
}

//MARK: - Calendar
extension Calendar {
    func numberOfDaysSince1970(to: Date) -> Int {
        let fromDate = startOfDay(for: Date(timeIntervalSince1970: 0))
        let toDate = startOfDay(for: to)
        let numberOfDays = dateComponents([.day], from: fromDate, to: toDate)
        
        return numberOfDays.day!
    }
}

//MARK: - CGPoint
extension CGPoint {
    func distance(to point: CGPoint) -> CGFloat {
        return (self.x - point.x) * (self.x - point.x) + (self.y - point.y) * (self.y - point.y)
    }
}

//MARK: - Date
let dateFormatter = DateFormatter()

extension Date {
    var yesterday: Date? {
        var dayComponent = DateComponents()
        dayComponent.day = -1
        
        return Calendar.current.date(byAdding: dayComponent, to: Date())
    }
    
    var chartStartDate: Date? {
        var dayComponent = DateComponents()
        dayComponent.day = -30
        
        return Calendar.current.date(byAdding: dayComponent, to: Date())
    }
    
    func chartViewFormatted() -> String {
        dateFormatter.dateFormat = "EEEE, dd MMMM"
        return dateFormatter.string(from: self)
    }
    
    func chartLegendFormatted() -> String {
        dateFormatter.dateFormat = "dd.MM"
        return dateFormatter.string(from: self)
    }
}

//MARK: - UIColor
extension UIColor {
    static let customGreen = UIColor(named: "CustomGreen")!
    static let customYellow = UIColor(named: "CustomYellow")!
    static let customLightGray = UIColor(named: "LightGray")!
    static let customMediumGray = UIColor(named: "MediumGray")!
}

//MARK: - UINavigationController
extension UINavigationController {
    open override func viewWillLayoutSubviews() {
        navigationBar.backgroundColor = .black
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationBar.topItem?.backButtonDisplayMode = .minimal
    }
}

//MARK: - View
extension View {
    func twoButtonsToolbar(leadingButtonTitle: String, trailingButtonTitle: String, _ leadingAction: @escaping (() -> Void), trailingAction: @escaping (() -> Void)) -> some View {
        self
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    ToolbarButton(title: leadingButtonTitle) { leadingAction() }
                }
            }
            .toolbar {
                ToolbarButton(title: trailingButtonTitle) { trailingAction() }
            }
    }
}

