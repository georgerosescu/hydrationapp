//
//  AppDefaults.swift
//  HydrationApp
//
//  Created by George Rosescu on 29.01.2022.
//

import Foundation

@propertyWrapper
struct AppDefaults<T> {
    private let key: String
    private let defaultValue: T
    
    init(key: String, defaultValue: T) {
        self.key = key
        self.defaultValue = defaultValue
    }
    
    var wrappedValue: T {
        get { UserDefaults.standard.object(forKey: key) as? T ?? defaultValue }
        set { UserDefaults.standard.set(newValue, forKey: key) }
    }
}

struct Defaults {
    
    @AppDefaults(key: Constants.Storage.measurementUnit, defaultValue: MeasurementUnit.metric.rawValue)
    static var measurementUnit: String
    
    @AppDefaults(key: Constants.Storage.dailyGoalValue, defaultValue: "2000")
    static var dailyGoalValue: String
    
    @AppDefaults(key: Constants.Storage.container1Value, defaultValue: "200")
    static var container1Value: String
    
    @AppDefaults(key: Constants.Storage.container2Value, defaultValue: "400")
    static var container2Value: String
    
    @AppDefaults(key: Constants.Storage.container3Value, defaultValue: "500")
    static var container3Value: String
    
    
    @AppDefaults(key: "inset", defaultValue: false)
    static var insertTest: Bool
}
