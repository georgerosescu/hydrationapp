//
//  GlassContainer.swift
//  HydrationApp
//
//  Created by George Rosescu on 25.01.2022.
//

import SwiftUI

struct GlassContainer: View {
    let dailyGoal: DailyIntakeGoal
    
    var body: some View {
        VStack(alignment: .center) {
            Text("\(dailyGoal.donePercent)%")
                .font(.system(size: 36, weight: .bold))
                .foregroundColor(Color(uiColor: .customGreen))
            Text("of \(dailyGoal.goalAndMeasurementUnit) Goal")
                .font(.system(size: 14))
                .foregroundColor(.white)
            GlassView(dailyGoal.todayIntakeValue, shapeFilledAmountpe: dailyGoal.glassShapeFilledAmount)
        }
    }
}

struct GlassContainer_Previews: PreviewProvider {
    static var previews: some View {
        GlassContainer(dailyGoal: DailyIntakeGoal())
    }
}
