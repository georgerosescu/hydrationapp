//
//  GlassView.swift
//  HydrationApp
//
//  Created by George Rosescu on 26.01.2022.
//

import SwiftUI

struct GlassView: View {
    let filledValue: String
    let shapeFilledAmountpe: Double
    
    init(_ filledValue: String, shapeFilledAmountpe: Double) {
        self.filledValue = filledValue
        self.shapeFilledAmountpe = shapeFilledAmountpe
    }
    
    var body: some View {
        Image("glass_empty")
            .renderingMode(.template)
            .background(
                GeometryReader { reader in
                    ZStack(alignment: .bottom) {
                        VStack {
                            Spacer()
                            Color.white.opacity(0.4)
                                .frame(height: reader.size.height * shapeFilledAmountpe, alignment: .bottom)
                        }
                        .frame(height: reader.size.height)
                        .clipShape(GlassShape())
                        Text(filledValue)
                            .padding(10)
                            .font(.system(size: 14))
                            .foregroundColor(.white)
                    }
                }
            )
    }
}

struct GlassShape: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        let originPoint = CGPoint(x: rect.minX, y: rect.minY)
        path.move(to: originPoint) // set path start position
        
        let minToMid = sqrt(originPoint.distance(to: CGPoint(x: rect.midX, y: rect.minY)))
        let downLeftPoint = CGPoint(x: minToMid / 2 - 5, y: rect.maxY)
        
        path.addLine(to: originPoint) //top
        path.addLine(to: downLeftPoint) //bottom
        path.addLine(to: CGPoint(x: rect.maxX - downLeftPoint.x, y: rect.maxY)) //bottom
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY)) //top
        
        return path
    }
}

struct GlassView_Previews: PreviewProvider {
    static var previews: some View {
        GlassView("200 ml", shapeFilledAmountpe: 0.2)
    }
}
