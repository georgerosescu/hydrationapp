//
//  WaterIntakeCell.swift
//  HydrationApp
//
//  Created by George Rosescu on 25.01.2022.
//

import SwiftUI

struct WaterIntakeCell: View {
    let amount: String
    let action: (() -> Void)
    
    var body: some View {
        Button {
           action()
        } label: {
            Text(amount)
                .frame(maxWidth: 80)
                .fixedSize()
                .truncationMode(.middle)
                .font(.system(size: 16))
                .padding(.horizontal, 30)
                .padding(.vertical, 9)
                .foregroundColor(.black)
                .background(
                    RoundedRectangle(cornerRadius: 4)
                        .tint(Color(uiColor: .customGreen))
                )
        }
    }
}

struct WaterIntakeCell_Previews: PreviewProvider {
    static var previews: some View {
        WaterIntakeCell(amount: "200 ml") {
            print("200 ml")
        }
    }
}
