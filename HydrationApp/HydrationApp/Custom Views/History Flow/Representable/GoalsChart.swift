//
//  GoalsChart.swift
//  HydrationApp
//
//  Created by George Rosescu on 29.01.2022.
//

import Foundation
import SwiftUI
import Charts

struct GoalsChart: UIViewRepresentable {
    let chartData: [CustomBarChartEntry]
    
    func makeUIView(context: Context) -> BarChartView {
        let chart = BarChartView()
        chart.chartDescription?.enabled = false
        chart.xAxis.drawGridLinesEnabled = false
        chart.xAxis.drawAxisLineEnabled = false
        chart.rightAxis.enabled = false
        chart.drawBordersEnabled = false
        chart.legend.form = .none
        chart.xAxis.labelPosition = .bottom
        
        return chart
    }
    
    func updateUIView(_ uiView: BarChartView, context: Context) {
        let dataSet = BarChartDataSet(entries: chartData)
        dataSet.label = nil
        
        uiView.data = BarChartData(dataSet: dataSet)
        formatDataSet(dataSet: dataSet)
        formatLeftAxis(uiView.leftAxis)
        formatXAxis(uiView.xAxis)
    }
}

//MARK: - Configuration Helpers
extension GoalsChart {
    //MARK: Left Axis
    private func formatLeftAxis(_ leftAxis: YAxis) {
        leftAxis.axisMinimum = 0
        leftAxis.axisMaximum = chartData.maximumChartValue
        leftAxis.axisLineColor = .clear
    }
    
    //MARK: XAxis
    private func formatXAxis(_ xAxis: XAxis) {
        var xAxisLabels = [String]()
        
        chartData.forEach { xAxisLabels.append($0.date) }
        xAxis.valueFormatter = IndexAxisValueFormatter(values: xAxisLabels)
        xAxis.setLabelCount(chartData.count, force: false)
    }
    
    //MARK: Data Set
    private func formatDataSet(dataSet: BarChartDataSet) {
        dataSet.colors = []
        dataSet.valueTextColor = .clear
        
        chartData.forEach {
            if let targetGoal = $0.targetGoal {
                dataSet.colors.append(Int($0.y) >= targetGoal ? .customGreen : .customYellow)
            } else {
                dataSet.colors.append(.lightGray)
            }
        }
    }
}
