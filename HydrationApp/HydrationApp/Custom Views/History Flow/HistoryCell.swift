//
//  HistoryCell.swift
//  HydrationApp
//
//  Created by George Rosescu on 28.01.2022.
//

import SwiftUI

struct HistoryCell: View {
    let date: Date
    let goal: String
    let actualAmount: String
    let donePercent: String
    let isDone: Bool
    
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 8) {
                Text(date.chartViewFormatted())
                    .foregroundColor(Color(uiColor: .customLightGray))
                    .font(.system(size: 16, weight: .regular))
                Text(actualAmount)
                    .font(.system(size: 18, weight: .semibold))
                    .foregroundColor(.white)
                HStack {
                    Text(donePercent)
                        .foregroundColor(.white)
                        .font(.system(size: 16, weight: .semibold))
                    Text("out of \(goal) Goal")
                        .foregroundColor(Color(uiColor: .customLightGray))
                        .font(.system(size: 16, weight: .regular))
                }
            }
            .padding(.leading, 20)
            Spacer()
            if isDone {
                Image("checkmark_goal")
                    .padding(.horizontal, 10)
            }
        }
    }
}

struct HistoryCell_Previews: PreviewProvider {
    static var previews: some View {
        HistoryCell(date: Date(), goal: "2000 ml", actualAmount: "1500 ml", donePercent: "100%", isDone: true)
            .preferredColorScheme(.dark)
    }
}
