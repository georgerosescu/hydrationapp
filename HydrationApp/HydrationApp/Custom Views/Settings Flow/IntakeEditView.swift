//
//  IntakeEditView.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import SwiftUI

struct IntakeEditView: View {
    let editViewType: EditViewType
    let measurementUnitTitle: String
    @Binding var value: String
    
    var body: some View {
        ZStack {
            Image("leaf_bg")
                .resizable()
                .overlay(Color(uiColor: .black).opacity(0.4))
            VStack {
                Text(editViewType.isContainer ? "\(Constants.containerUpperText)" : "\(Constants.dailyGoalUpperText)")
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .padding(.top, 20)
                
                Spacer()
                    .frame(height: 120)
                VStack {
                    CustomTextField(input: $value)
                        .frame(maxWidth: 250)
                        .fixedSize()
                    Text(measurementUnitTitle)
                        .font(.system(size: 18, weight: .semibold))
                        .foregroundColor(.white)
                        .padding(.top, 10)
                }
                Spacer()
            }
            .padding(.horizontal, 80)
        }
    }
}

struct IntakeEditView_Previews: PreviewProvider {
    static var previews: some View {
        IntakeEditView(editViewType: .daily, measurementUnitTitle: MeasurementUnit.imperial.longTitle, value: .constant("2200"))
            .preferredColorScheme(.dark)
    }
}
