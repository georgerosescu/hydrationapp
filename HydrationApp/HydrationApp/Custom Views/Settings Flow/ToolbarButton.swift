//
//  ToolbarButton.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import SwiftUI

struct ToolbarButton: View {
    let title: String
    let action: (() -> Void)
    
    var body: some View {
        Button {
            action()
        } label: {
            Text(title)
                .font(.system(size: 17))
        }
    }
}

struct ToolbarButton_Previews: PreviewProvider {
    static var previews: some View {
        ToolbarButton(title: "Save") {
            print("Save item")
        }
    }
}
