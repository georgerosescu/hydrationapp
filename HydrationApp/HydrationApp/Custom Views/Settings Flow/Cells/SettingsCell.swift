//
//  SettingsCell.swift
//  HydrationApp
//
//  Created by George Rosescu on 28.01.2022.
//

import SwiftUI

struct SettingsCell: View {
    let leftText: String
    let rightText: String
    
    var body: some View {
        HStack {
            Text(leftText)
            Spacer()
            Text(rightText)
        }
        .font(.system(size: 16))
        .foregroundColor(.white)
    }
}

struct SettingsCell_Previews: PreviewProvider {
    static var previews: some View {
        SettingsCell(leftText: "Units", rightText: "ml")
            .preferredColorScheme(.dark)
    }
}
