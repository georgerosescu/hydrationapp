//
//  MeasurementUnitCell.swift
//  HydrationApp
//
//  Created by George Rosescu on 28.01.2022.
//

import SwiftUI

struct MeasurementUnitCell: View {
    let measurementUnit: MeasurementUnit
    @Binding var selectedMeasurementUnit: MeasurementUnit
    
    var body: some View {
        HStack {
            Text(measurementUnit.longTitle)
            Spacer()
            if measurementUnit == selectedMeasurementUnit {
                Image("checkmark_goal")
                    .resizable()
                    .frame(width: 22, height: 22)
            }
        }
    }
}

struct MeasurementUnitCell_Previews: PreviewProvider {
    static var previews: some View {
        MeasurementUnitCell(measurementUnit: MeasurementUnit.imperial, selectedMeasurementUnit: .constant(MeasurementUnit.imperial))
            .preferredColorScheme(.dark)
    }
}
