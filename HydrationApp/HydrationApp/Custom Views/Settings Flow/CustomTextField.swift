//
//  CustomTextField.swift
//  HydrationApp
//
//  Created by George Rosescu on 27.01.2022.
//

import SwiftUI

struct CustomTextField: View {
    @Binding var input: String
    
    var body: some View {
        VStack {
            TextField("Intake", text: $input)
                .padding(.horizontal, 20)
                .multilineTextAlignment(.center)
                .padding(.vertical, 10)
                .keyboardType(.numberPad)
                .font(.system(size: 36, weight: .light))
                .foregroundColor(.white)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color(uiColor: .customGreen), lineWidth: 4)
                )
        }
    }
}

struct CustomTextField_Previews: PreviewProvider {
    static var previews: some View {
        CustomTextField(input: .constant("2000"))
            .preferredColorScheme(.dark)
    }
}
