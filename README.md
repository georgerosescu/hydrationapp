# README #
This repo contains my app implementation for the Garmin Code Challenge.
**HydratationApp**
![Scheme](HydrationApp/HydrationApp/Assets.xcassets/AppIcon.appiconset/58.png?raw=true)
### Implementation ###

The app is implemented using SwiftUI 3 and has iOS 15.0 as minimum target.
Third party libs used(via SPM): 
1. Realm Swift: https://github.com/realm/realm-swift
2. Charts: https://github.com/danielgindi/Charts

Please use a device that runs min iOS 15 in order to run the application, from the master branch.



